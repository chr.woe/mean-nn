
%% This function calculates the induced electric field of a C-B60 (MagVenture) TMS coil in a 2D plane.

%% This script was adapted and modified from the  -coil05_tester_plane_transverse_e.m-  script :
%% Reference:
% Makarov SN, Wartman WA, Daneshzand M, Fujimoto K, Raij T, Nummenmaa A.
% A software toolkit for TMS electric-field modeling with boundary element fast multipole method: 
% an efficient MATLAB implementation. J Neural Eng. 2020 Aug 4;17(4):046023. 
% doi: 10.1088/1741-2552/ab85b3. PMID: 32235065.



% The modified function needs to ebedded into the simulation envrionment of Makarvov et
% al. 2020, analogically in the same manner as the
% -coil05_tester_plane_transverse_e.m- script
%For further informations see also supplementary material of Makarov et al. 2020: 
% https://iopscience.iop.org/article/10.1088/1741-2552/ab85b3

%--------------------------------------------------------------------------------------------------------------------%






function [f, g] = call_Ex_field(Xfunc, Yfunc, angle, component, size,  visualize)
    %clear all; %#ok<CLALL>
    if ~isunix
        s = pwd; addpath(strcat(s(1:end-5), '\Engine'));
    else
        s = pwd; addpath(strcat(s(1:end-5), '/Engine'));
    end

    %  Load coil data (from the Makarov et al. environment)
    load coil.mat;
    load coilCAD.mat;
    clear pointsXY;


    %%   Define coil position: rotate and then tilt and move the entire coil as appropriate
    coilaxis        = [0 0 1];                  %   Transformation 1: rotation axis
    theta           = angle;                        %   Transformation 1: angle to rotate about axis
    %Nx = +0.45; Ny = 0.0; Nz = 1.0;             %   Transformation 2: New coil centerline direction
    %MoveX = +42e-3; MoveY = 0; MoveZ = 79.5e-3; %   Transformation 3: New coil position

    %   Apply Transformation 1: rotation about coil centerline
    strcoil.Pwire   = meshrotate2(strcoil.Pwire, coilaxis, theta);
    P          = meshrotate2(P, coilaxis, theta);

    %   Apply Transformation 2: Tilt the coil axis with direction vector Nx, Ny, Nz as required
    %strcoil.Pwire = meshrotate1(strcoil.Pwire, Nx, Ny, Nz);
    %P        = meshrotate1(P, Nx, Ny, Nz);







    %%  Coil parameters
    %  Define dIdt (for electric field)
    dIdt = 15.5e7;       %   Amperes/sec (2*pi*I0/period)
    %  Define I0 (for magnetic field)
    I0 = 1;       %   Amperes

    zposCoil = 0.0148; % see Lu et al. 2019 doi: 10.1111/cns.13204
    %  Parameters
    mu0     = 1.25663706e-006;  %   magnetic permeability of vacuum(~air)
    levels  = 40;               %   number of levels in contour plot
    comp    = component;                %   field component to be plotted (1, 2, 3 or x, y, z)
    temp        = ['x' 'y' 'z'];
    label       = temp(comp);

    %   Plane window (from xmin to xmax and from zmin to zmax)
    scale = 3.5;
    Xmin = min(strcoil.Pwire(:, 1));
    Xmax = max(strcoil.Pwire(:, 1));
    Ymin = min(strcoil.Pwire(:, 2));
    Ymax = max(strcoil.Pwire(:, 2));
    xmin = scale*Xmin;
    xmax = scale*Xmax;
    ymin = scale*Ymin;
    ymax = scale*Ymax ;
    Z    = min(strcoil.Pwire(:, 3))-zposCoil;          %  position of the XY plane 

    if visualize == true
        %   Plot the plane
        f1 = figure;
        bemf1_graphics_coil_CAD(P, t, 0);
        patch([xmin xmin xmax xmax], [ymin ymax ymax ymin], [Z Z Z Z], 'c', 'FaceAlpha', 0.25);
        hold on;
        plot3(Xfunc, Yfunc, (-zposCoil), 'r*');
        view(30, 30);
        hold off;
    end
    
    %  Nodal points on the surface (MsxMs nodal points)
        Ms = size; %250
        %   Coronal plane
        x = linspace(xmin, xmax, Ms);
        y = linspace(ymin, ymax, Ms);
        [X, Y]  = meshgrid(x, y);
        pointsXY(:, 1) = reshape(X, 1, Ms^2);
        pointsXY(:, 2) = reshape(Y, 1, Ms^2);
        pointsXY(:, 3) = Z*ones(1, Ms^2);
    
    tic
    
    
    %%%%%%%%%%%%%%%%%%%%%
    Field        = bemf3_inc_field_electric(strcoil, pointsXY, dIdt, mu0);
    max_Field = max(Field, [], 1);
    norm_Field = Field(:, comp);  %./max_Field(comp);
    
    %%%%%%%%%%%%%%%%%%%%%%%
    
    G = reshape(norm_Field(:,1), [size,size,1]);
    
    deltax = (xmax-xmin)/Ms;
    deltay = (ymax-ymin)/Ms;
   
    x_rel = round(Xfunc/deltay)+size/2;
    y_rel = round(Yfunc/deltax)+size/2;
    %%%%%%%%%%%%%%%%%%%%%
    
    
    
    fieldPlaneTime = toc



    if visualize == true  %visualization is only for plotting
        %  Plot field on the surface (transverse plane)
        f2      = figure;
        temp    = I0*norm_Field;
        %temp = fie(:, 1);
        th1     = max(temp);             %   threshold for magnetic-field plot, T
        th2     = min(temp);             %   threshold for magnetic-field plot, T
        bemf2_graphics_vol_field(temp, th1, th2, levels, x, y);
        hold on;
        plot(Xfunc, Yfunc,  'r*');
        xlabel('x, [m]'); ylabel('y, [m]');
        colormap parula; colorbar;
        %%clim([-1.25e-6 2e-6]) for setting the colorbar
        title(strcat('E-field [V/m], ', label, '-component in the transverse plane'));

        %   Additionally, plot coil cross-section
        [edges, TriP, TriM] = mt(t);
        [Pi, ti, polymask, flag] = meshplaneintXY(P, t, edges, TriP, TriM, Z);
        if flag % intersection found
            for n = 1:size(polymask, 1)
                i1 = polymask(n, 1);
                i2 = polymask(n, 2);
                line(Pi([i1 i2], 1), Pi([i1 i2], 2), 'Color', 'r', 'LineWidth', 2);
            end
        end

        grid on; set(gcf,'Color','White');
        axis equal; axis tight;
        hold off;
    end
   
     
   f = G(x_rel, y_rel);
   g = max_Field(1, 2);
   
end