
%% In this script the main functionality of the pseudo-randomized virtual TMS-MEP model is implemented.

%This script uses implementations modified and adapted from:
%% Reference 1):
% 'call_Ex_field (...)' is a function implemented in a separate script, and was modified from:

% Makarov SN, Wartman WA, Daneshzand M, Fujimoto K, Raij T, Nummenmaa A.
% A software toolkit for TMS electric-field modeling with boundary element fast multipole method: 
% an efficient MATLAB implementation. J Neural Eng. 2020 Aug 4;17(4):046023. 
% doi: 10.1088/1741-2552/ab85b3. PMID: 32235065.


%%  Reference 2):
%'virtualsubjectEIVGenerateSubject' and 'virtstimulate(norm_field(j), subject(1).parameters)' 
% uses functions implemented in the statistical environment adapted from: 

% Goetz, S. M., Alavi, S. M. M., Deng, Z.-D., & Peterchev, A. V. (2019).
% Statistical Model of Motor-Evoked Potentials. 
% IEEE Transactions on Neural Systems and Rehabilitation Engineering, 27(8), 1539–1545. 
% https://doi.org/10.1109/TNSRE.2019.2926543

%See also: https://github.com/sgoetzduke/Statistical-MEP-Model



%% Generate random subject and stimulates on a specific coil position in the 2D air-plane
clear all;
rng('shuffle')

%create subject 1
subject(1).parameters = virtualsubjectEIVGenerateSubject;

%% Random number Matrix: numb_pos Coil Positions in 2D plane - in Polar coordinates arround ROI normed between 0,1 (length on radius | angle | angle of coil rotation)  
numb_pos = 20;
StimPos = rand(numb_pos ,3); %[0.8, 0.5, 0.5];   %[0, 0.5, 0];   %rand(numb_pos ,3); %[0, 0.5, 0];   %    0.5 ist beides mal 0° 



%% Definition of parameters
radius = 0.035;
angle_range = pi/1.5; % overall +/- 90°  
%initial point region of interest
x_roi_spot = 0.0;
y_roi_spot = 0.0;


%% this creates discrete predefined Stimsites
% StimPos(:,1) = [0.0] ;
% StimPos(:,2) = [0 ];
% StimPos(:,3) = [0];

%% this is for random generation
%renormation of initial random Stimulation Position
StimPos(:,1) = StimPos(:,1)*radius;
StimPos(:,2) = StimPos(:,2)*2*pi;
StimPos(:,3) = angle_range*StimPos(:,3)-angle_range/2;


%Transform Position to Kartesian
[x_rand_pos, y_rand_pos] = pol2cart(StimPos(:,2)+pi/2, StimPos(:,1));   % !!!! 3.141/2
%Transform angle to Kartesian
[U, V] = pol2cart(StimPos(:,3)+pi/2, 0.007); % PI addiert für Veranschaulichung verglichen mit Coil Bild    !!! +3.141/2

%% Plot the ROI with the different coil positions and orientations
f1 = figure('Name', 'Different coil positions and orientations')
q = quiver(x_rand_pos, y_rand_pos, U, V, 0);
q.LineWidth = 1;
q.Color = 'blue';
q.Marker = '.';
axis equal;
hold on;
plot(x_roi_spot, y_roi_spot, '*', 'MarkerSize', 12, 'LineWidth', 2);

%plot ROI
th = 0:pi/360:2*pi;
xunit = radius * cos(th) + x_roi_spot;
yunit = radius * sin(th) + y_roi_spot;
plot(xunit, yunit,  'Color', 'yellow');
drawnow
hold off;

%% %%%%%%%%





resolution = 500;
component = 2; % 1 is x-component 
for i = 1:numb_pos
    x_spot = -x_rand_pos(i);
    y_spot = -y_rand_pos(i);
    coil_orientation = StimPos(i,3); %in 2PI format
    if i==round(numb_pos/2)  %visualize the position at half of stim_numb
        visualize = true;
    else
        visualize = false;
    end
    %visualize = true;
    
    %E - Field - modified from Makarov et al. 2020
    [E_val, max_E_val] = call_Ex_field(x_spot, y_spot, coil_orientation, component, resolution, visualize); 
    field(1,i) = E_val;
    if max_E_val>0
        field(2,i) = max_E_val;
    else 
        field(2,i) = 0;
    end
%Invertierung der Feldrichtung für Berechnung des MEP - da die
%Stromrichtung durch Coil im Zentrum max NEGATIV ist hier jedoch am meisten
%MEP entsteht
    %field(i) = -field(i);

    if field(1, i) < 0
        field(1, i) = 0;
    end
    
    
end
overall_E_max = max(field(2,:));
for j = 1:numb_pos
    norm_field(j) = field(1,j)/overall_E_max;
    MEPVpp(j) = virtstimulate(norm_field(j), subject(1).parameters);  %adapted and modified from Goetz et al. 2019
end


[maxMEP, maxIndex] = max(MEPVpp);








%% %%%%%%%%%%%   
%% all the plotting

%%
close(f1); %close figure 1

%% Plot the ROI with the different coil positions and orientations
f4 = figure('Name', 'Best coil Position');
s = quiver(x_rand_pos, y_rand_pos, U, V, 0);
s.LineWidth = 1;
s.Color = 'blue';
s.Marker = '.';
axis equal;
hold on;
plot(x_roi_spot, y_roi_spot, '*', 'MarkerSize', 12, 'LineWidth', 2);

w = quiver(x_rand_pos(maxIndex), y_rand_pos(maxIndex), U(maxIndex), V(maxIndex), 0);
w.LineWidth = 1;
w.Color = 'green';
w.Marker = 'h';
w.MarkerSize = 10;
w.LineStyle = '--';
w.ShowArrowHead = 'off';
axis([-0.06 0.06 -0.06 0.06]);
axis equal;

%plot ROI
th = 0:pi/360:2*pi;
xunit = radius * cos(th) + x_roi_spot;
yunit = radius * sin(th) + y_roi_spot;
plot(xunit, yunit, 'Color', 'yellow');

plot3(x_rand_pos, y_rand_pos, MEPVpp, 'x', 'MarkerSize', 7);
hold off;

% x_simul = StimPos(:,1);
% y_simul = StimPos(:,3)*360/(2*pi);
 z_simul = MEPVpp*1000;
 c = sqrt(z_simul);
% figure(10)
% scatter3(x_simul,y_simul,z_simul, 30 , c, 'filled');
% title('MEP-distribution depending on distance and angle')
% xlabel('Distance from calculated target position (in mm)')
% ylabel('Rotation angle compared to target position (in °)')
% zlabel('MEP (peak to peak in mV)')




figure(4)
scatter3(x_rand_pos,y_rand_pos,z_simul, 30 , c, 'filled');
title('MEP-distribution depending on distance and angle')
xlabel('x (in mm)')
ylabel('y (in mm')
zlabel('MEP (peak to peak in mV)')





