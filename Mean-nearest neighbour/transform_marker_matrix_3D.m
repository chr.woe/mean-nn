%% This function is a helper function
% it implements the Geometric complexity reduction presented in the thesis from C. W0ERNLE 
% it uses two external geometric transformation functions (References given below)

function [dist, rot, direct, max_angle] = transform_marker_matrix_3D(eval_matrix, target_matrix, plotting, marker_number_plot )


origin = [0,0,0];
koord_target = [target_matrix(4).Value,target_matrix(8).Value,target_matrix(12).Value];
koord_eval = [eval_matrix(4,1),eval_matrix( 8,1),eval_matrix(12,1)];

%koord_target = [3,3,3]; %for testing
%koord_eval = [3,3,3]; %for testing

e1 = [2,0,0];
e2 = [0,2,0];
e3 = [0,0,2];

%% Matrix init and transformation
%% ________________________________________________________________________________________________________________________________________________________________________
%Pay attention sometimes localite Data is flipped 180° such that it's
%easier to aquire data in the lab. 

%% Attention hdf5 files are already in simnibs coord --> xml is in Localite system
%Be careful here ToRight and Head are interchanged if .hdf5 files are used from SIMNIBS,  Localite and SIMNIBS coordinate systems are different
Head_target_vec  =   [target_matrix(1).Value, target_matrix(5).Value, target_matrix(9).Value];
Handhold_target_vec  = [target_matrix(2).Value, target_matrix(6).Value, target_matrix(10).Value];   % Check localite - simnibs
ToRight_target_vec =  [target_matrix(3).Value, target_matrix(7).Value, target_matrix(11).Value];  % 

%%  
Head_eval_vec =     [eval_matrix(1, 1), eval_matrix(5, 1), eval_matrix(9,1) ];
Handhold_eval_vec = [eval_matrix(2,1), eval_matrix(6,1), eval_matrix(10,1)];  
ToRight_eval_vec =  [eval_matrix(3,1), eval_matrix(7,1), eval_matrix(11,1) ];  

rotation_spec = vrrotvec(Head_target_vec, Head_eval_vec); %berechnet Rotationsachse zwischen zwei x-Spulenvektoren
                           %so bekommt man Spulenpositionen in eine Ebene, jedoch ohne die Rotation in der Ebene zu ändern

Head_eval_corrected = rotVecAroundArbAxis(Head_eval_vec./norm(Head_eval_vec), rotation_spec(1:3)./norm(rotation_spec(1:3)),-rad2deg(rotation_spec(4)));
Handhold_eval_corrected = rotVecAroundArbAxis(Handhold_eval_vec./norm(Handhold_eval_vec), rotation_spec(1:3)./norm(rotation_spec(1:3)),-rad2deg(rotation_spec(4)));
ToRight_eval_corrected = rotVecAroundArbAxis(ToRight_eval_vec./norm(ToRight_eval_vec), rotation_spec(1:3)./norm(rotation_spec(1:3)),-rad2deg(rotation_spec(4)));
%siehe Funktion unten (diese dreht die Spulenposition von der evaluierten Position zur Position des Hotspots/Targets in der x-Richtung (Tangential zur Kopfoberfläche (vereinfachte Annahme)

%Absolute Werte siehe M - Matrix letzte Spalte (Abs Koordinate wird auf
%addiert)
Head_target_abs = Head_target_vec + koord_target;
Handhold_target_abs = Handhold_target_vec + koord_target;
ToRight_target_abs = ToRight_target_vec + koord_target;

Head_eval_abs = Head_eval_vec + koord_eval;
Handhold_eval_abs = Handhold_eval_vec + koord_eval;
ToRight_eval_abs = ToRight_eval_vec + koord_eval;

Head_eval_corrected_abs = Head_eval_corrected + koord_eval;
Handhold_eval_corrected_abs = Handhold_eval_corrected + koord_eval;
ToRight_eval_corrected_abs = ToRight_eval_corrected + koord_eval;


%% _____________________________________________________________________________________________________________________________________________________________________
%%


%% Plotting the transformation of one example stimulation site for verification

if plotting
    figure;
    hold on;

    %koord achses
%     plot3([origin(1) e1(1)],[origin(2) e1(2)],[origin(3) e1(3)],'r-^', 'LineWidth',4);
%     plot3([origin(1) e2(1)],[origin(2) e2(2)],[origin(3) e2(3)],'y-^', 'LineWidth',4);
%     plot3([origin(1) e3(1)],[origin(2) e3(2)],[origin(3) e3(3)],'g-^', 'LineWidth',4);


     plot3([koord_target(1) Head_target_abs(1)],[koord_target(2) Head_target_abs(2)],[koord_target(3) Head_target_abs(3)],'r-^', 'LineWidth',3);
     plot3([koord_target(1) Handhold_target_abs(1)],[koord_target(2) Handhold_target_abs(2)],[koord_target(3) Handhold_target_abs(3)],'g-^', 'LineWidth',3);
     plot3([koord_target(1) ToRight_target_abs(1)],[koord_target(2) ToRight_target_abs(2)],[koord_target(3) ToRight_target_abs(3)],'b-^', 'LineWidth',3);

    plot3([koord_eval(1) Head_eval_abs(1)],[koord_eval(2) Head_eval_abs(2)],[koord_eval(3) Head_eval_abs(3)],'m-^', 'LineWidth',1);
    plot3([koord_eval(1) Handhold_eval_abs(1)],[koord_eval(2) Handhold_eval_abs(2)],[koord_eval(3) Handhold_eval_abs(3)],'y-^', 'LineWidth',1);
    plot3([koord_eval(1) ToRight_eval_abs(1)],[koord_eval(2) ToRight_eval_abs(2)],[koord_eval(3) ToRight_eval_abs(3)],'k-^', 'LineWidth',1);

     plot3([koord_eval(1) Head_eval_corrected_abs(1)],[koord_eval(2) Head_eval_corrected_abs(2)],[koord_eval(3) Head_eval_corrected_abs(3)],'r-^', 'LineWidth',1);
     plot3([koord_eval(1) Handhold_eval_corrected_abs(1)],[koord_eval(2) Handhold_eval_corrected_abs(2)],[koord_eval(3) Handhold_eval_corrected_abs(3)],'g-^', 'LineWidth',1);
     plot3([koord_eval(1) ToRight_eval_corrected_abs(1)],[koord_eval(2) ToRight_eval_corrected_abs(2)],[koord_eval(3) ToRight_eval_corrected_abs(3)],'b-^', 'LineWidth',1);

 

    grid on;
    xlabel('X axis'), ylabel('Y axis'), zlabel('Z axis')
    title(sprintf('Example TriggerMarker Position %d and target position', marker_number_plot))
    legend(  'To Head Center', 'ToHandhold Center ','ToRight Center','ToRight Marker correct' , 'ToHead Marker correct',  'ToRight Marker correct')
    set(gca,'CameraPosition',[1 2 3]);
    hold off;
end;


%% angle calculation
angle_head_hand = rad2deg(atan2(norm(cross(Head_eval_corrected,Handhold_eval_corrected)), dot(Head_eval_corrected,Handhold_eval_corrected)));
angle_hand_right = rad2deg(atan2(norm(cross(ToRight_eval_corrected,Handhold_eval_corrected)), dot(ToRight_eval_corrected,Handhold_eval_corrected)));
angle_head_right = rad2deg(atan2(norm(cross(Head_eval_corrected,ToRight_eval_corrected)), dot(Head_eval_corrected,ToRight_eval_corrected)));
angle_3 = rad2deg(atan2(norm(cross(Handhold_target_vec,Handhold_eval_corrected)), dot(Handhold_target_vec,Handhold_eval_corrected)));
angle_4 = rad2deg(atan2(norm(cross(ToRight_target_vec,ToRight_eval_corrected)), dot(ToRight_target_vec,ToRight_eval_corrected)));
angle_5 = rad2deg(atan2(norm(cross(Head_target_vec,Head_eval_vec)), dot(Head_target_vec,Head_eval_vec)));
%______________________________________________


%% Berechne 2D Ebene (Polar Koordinaten) transform everything in 2D plane

koord_eval_2D_abs = [koord_eval(1), koord_eval(2), koord_target(3)];
koord_eval_2D_zero = koord_eval_2D_abs - koord_target;
Handhold_target_vec_2D = [Handhold_target_vec(1), Handhold_target_vec(2), 0];
angle_direction = vecangle360(Handhold_target_vec_2D, koord_eval_2D_zero, e3);


%% Relevanter Winkel mit Vorzeichen - Rotation zwischen Target und Evaluated (siehe externe Funktion)
angle_rot = vecangle360(Handhold_target_vec, Handhold_eval_corrected, -Head_target_vec);


%% Return values
dist = sqrt((koord_target(1)-koord_eval(1))^2+(koord_target(2)-koord_eval(2))^2+ (koord_target(3)-koord_eval(3))^2 );
rot = angle_rot;
direct = angle_direction;
max_angle = angle_5;
end












%% EXTERNAL FUNCTIONS
%%
%% external angle & angle-sign calculation

%Reference:
% https://de.mathworks.com/matlabcentral/answers/501449-angle-betwen-two-3d-vectors-in-the-range-0-360-degree
%author James Tursa, 23 Jan 2020

%This is a simple (non-vectorized) function that takes two input vectors v1 and v2, 
%and a vector n that is not in the plane of v1 & v2.  
%Here n is used to determine the "direction" of the angle between v1 and v2 in a right-hand-rule sense.  
%I.e., cross(n,v1) would point in the "positive" direction of the angle starting from v1. 

function a = vecangle360(v1,v2,n)
x = cross(v1,v2);
c = sign(dot(x,n)) * norm(x);
a = atan2d(c,dot(v1,v2));
end









%% external rotation function

function rotatedUnitVector = rotVecAroundArbAxis(unitVec2Rotate,rotationAxisUnitVec,theta)

%
%% References:
%  Murray, G. Rotation About an Arbitrary Axis in 3 Dimensions. 06/06/2013.
%  https://www.mathworks.com/matlabcentral/fileexchange/49916-rotvecaroundarbaxis-unitvec2rotate-rotationaxisunitvec-theta


%% Purpose:
%  This routine will allow a unit vector, unitVec2Rotate, to be rotated 
%  around an axis defined by the RotationAxisUnitVec.  This is performed by
%  first rotating the unit vector around it's own cartesian axis (in this
%  case we will rotate the vector around the z-axis, [0 0 1]) corresponding
%  to each rotation angle specified by the user via the variable theta ...
%  this rotated vector is then transformed around the user defined axis of
%  rotation as defined by the rotationAxisUnitVec variable.
%  
%



%% Inputs:
%  unitVec2Rotate               [N x 3]                 Unit Vector in
%                                                       Cartesian 
%                                                       Coordinates to 
%                                                       rotate
%                                                       [x,y,z]
%
%
%  rotationAxisUnitVec          [N x 3]                 Unit Vector with
%                                                       respect to the same 
%                                                       cartesian coordinates 
%                                                       used for unitVec2Rotate
%                                                       [x,y,z]
%
%  theta                        [N x 1]                 Angle in degrees
%                                                       in which to rotate
%                                                       the unitVec2Rotate
%                                                       about the Z-axis
%                                                       before transforming
%                                                       it to the
%                                                       RotateionAxisUnitVec
%                                                       This rotation is
%                                                       counter clockwise
%                                                       when theta is
%                                                       positive, clockwise
%                                                       when theta is
%                                                       negative.
%
%% Outputs:
%  rotatedUnitVector            [N x 3]                 Resulting vector
%                                                       of rotating the
%                                                       unitVec2Rotate
%                                                       about the z-axis
%                                                       described by the
%                                                       angle theta, then
%                                                       transforming the
%                                                       rotated vectors
%                                                       with respect to the
%                                                       rotateionAxisUnitVec
%
%% Revision History:    
%  Darin C. Koblick                                        (c)  03-03-2015
%
%  Darin C. Koblick      Fixed order of rotations               07-30-2015                                       


%% ---------------------- Begin Code Sequence -----------------------------
if nargin == 0
         unitVec2Rotate = [1 0 1]./norm([1 0 1]);
    rotationAxisUnitVec = [1 1 1]./norm([1 1 1]);
    theta = (0:5:360)';
    rotatedUnitVector = rotVecAroundArbAxis(unitVec2Rotate,rotationAxisUnitVec,theta);
    %Show a graphical representation of the rotated vector:
    figure('color',[1 1 1]);
    quiver3(zeros(numel(theta),1),zeros(numel(theta),1),zeros(numel(theta),1), ...
            rotatedUnitVector(:,1),rotatedUnitVector(:,2),rotatedUnitVector(:,3),'k','linewidth',2);
    hold on;
    quiver3(0,0,0,rotationAxisUnitVec(1), ...
                  rotationAxisUnitVec(2), ...
                  rotationAxisUnitVec(3), ...
                  'r','linewidth',5);
    axis equal;
    return;
end
%Check the dimensions of the input vectors to see if we need to repmat
%them:
if size(unitVec2Rotate,1) == 1
   unitVec2Rotate = repmat(unitVec2Rotate,[numel(theta) 1]); 
end
if size(rotationAxisUnitVec,1) == 1
   rotationAxisUnitVec = repmat(rotationAxisUnitVec,[numel(theta) 1]); 
end
%% Step One: take the unit vector rotation axis and rotate into z:
R2Z = vecRotMat(rotationAxisUnitVec,repmat([0 0 1],[size(rotationAxisUnitVec,1) 1]));
unitVectortoRotateAboutZ =Dim33Multiply(unitVec2Rotate,R2Z);
% Rotate the unit vector about the z-axis:
rotatedAboutZAxisUnitVec = bsxRz(unitVectortoRotateAboutZ,theta.*pi/180); 
%% Step Two: Find the rotation Matrix to transform the z-axis to rotationAxisUnitVec
R = vecRotMat(repmat([0 0 1],[size(rotationAxisUnitVec,1) 1]),rotationAxisUnitVec);
%% Step Three: Apply the Rotation matrix to the rotatedAboutZAxisUnitVec vectors
rotatedUnitVector =Dim33Multiply(rotatedAboutZAxisUnitVec,R);
end
function a = Dim33Multiply(a,b)
%% Purpose:
% Given a, an [N x 3] matrix, use b, an [3 x 3 x N] rotation matrix to come
% up with a vectorized solution to b*a
%
%% Inputs:
%  a            [N x 3]                                        N x 3 vector
%
%  b            [3 x 3 x N]                                    3 x 3 x N
%                                                              matrix
%
%% Outputs:
%  a            [N x 3]                                        vectorized
%                                                              solution
%                                                              a = b*a
%
%% Revision History:
% Created by Darin C. Koblick   (C)                                    2013
%% ---------------------- Begin Code Sequence -----------------------------
a =cat(1,sum(permute(bsxfun(@times,b(1,:,:),permute(a,[3 2 1])),[2 3 1]),1), ...
         sum(permute(bsxfun(@times,b(2,:,:),permute(a,[3 2 1])),[2 3 1]),1), ...
         sum(permute(bsxfun(@times,b(3,:,:),permute(a,[3 2 1])),[2 3 1]),1))';
end
function R = vecRotMat(f,t)
%% Purpose:
%Commonly, it is desired to have a rotation matrix which will rotate one 
%unit vector, f,  into another unit vector, t. It is desired to 
%find R(f,t) such that R(f,t)*f = t.  
%
%This program, vecRotMat is the most
%efficient way to accomplish this task. It uses no square roots or
%trigonometric functions as they are very computationally expensive. 
%It is derived from the work performed by Moller and Hughes, which have
%suggested that this method is the faster than any previous transformation
%matrix methods tested.
%
%
%% Inputs:
%f                      [N x 3]                         N number of vectors
%                                                       in which to
%                                                       transform into
%                                                       vector t.
%
%t                      [N x 3]                         N number of vectors
%                                                       in which it is
%                                                       desired to rotate
%                                                       f.
%
%
%% Outputs:
%R                      [3 x 3 x N]                     N number of
%                                                       rotation matrices
%
%% Source:
% Moller,T. Hughes, F. "Efficiently Building a Matrix to Rotate One 
% Vector to Another", 1999. http://www.acm.org/jgt/papers/MollerHughes99
%
%% Created By:
% Darin C. Koblick (C) 07/17/2012
% Darin C. Koblick     04/22/2014       Updated when lines are close to
%                                       parallel by checking 
%% ---------------------- Begin Code Sequence -----------------------------
%It is assumed that both inputs are in vector format N x 3
dim3 = 2;
%Declare function handles for multi-dim operations
normMD = @(x,y) sqrt(sum(x.^2,y));
anyMD  = @(x) any(x(:));
% Inputs Need to be in Unit Vector Format
if anyMD(single(normMD(f,dim3)) ~= single(1)) || anyMD(single(normMD(t,dim3)) ~= single(1))
   error('Input Vectors Must Be Unit Vectors');
end
%Pre-Allocate the 3-D transformation matrix
R = NaN(3,3,size(f,1));
v = permute(cross(f,t,dim3),[3 2 1]);
c = permute(dot(f,t,dim3),[3 2 1]);
h = (1-c)./dot(v,v,dim3);
idx  = abs(c) > 1-1e-13;
%If f and t are not parallel, use the following computation
if any(~idx)
%For any vector u, the rotation matrix is found from:
R(:,:,~idx) = ...
    [c(:,:,~idx) + h(:,:,~idx).*v(:,1,~idx).^2,h(:,:,~idx).*v(:,1,~idx).*v(:,2,~idx)-v(:,3,~idx),h(:,:,~idx).*v(:,1,~idx).*v(:,3,~idx)+v(:,2,~idx); ...
     h(:,:,~idx).*v(:,1,~idx).*v(:,2,~idx)+v(:,3,~idx),c(:,:,~idx)+h(:,:,~idx).*v(:,2,~idx).^2,h(:,:,~idx).*v(:,2,~idx).*v(:,3,~idx)-v(:,1,~idx); ...
     h(:,:,~idx).*v(:,1,~idx).*v(:,3,~idx)-v(:,2,~idx),h(:,:,~idx).*v(:,2,~idx).*v(:,3,~idx)+v(:,1,~idx),c(:,:,~idx)+h(:,:,~idx).*v(:,3,~idx).^2];
end
%If f and t are close to parallel, use the following computation
if any(idx)
     f = permute(f,[3 2 1]);
     t = permute(t,[3 2 1]);
     p = zeros(size(f));
     iidx = abs(f(:,1,:)) <= abs(f(:,2,:)) & abs(f(:,1,:)) < abs(f(:,3,:));
     if any(iidx & idx)
        p(:,1,iidx & idx) = 1;
     end
     iidx = abs(f(:,2,:)) < abs(f(:,1,:)) & abs(f(:,2,:)) <= abs(f(:,3,:));
     if any(iidx & idx)
        p(:,2,iidx & idx) = 1;
     end
     iidx = abs(f(:,3,:)) <= abs(f(:,1,:)) & abs(f(:,3,:)) < abs(f(:,2,:));
     if any(iidx & idx)
        p(:,3,iidx & idx) = 1;
     end
     u = p(:,:,idx)-f(:,:,idx);
     v = p(:,:,idx)-t(:,:,idx);
     rt1 = -2./dot(u,u,dim3);
     rt2 = -2./dot(v,v,dim3);
     rt3 = 4.*dot(u,v,dim3)./(dot(u,u,dim3).*dot(v,v,dim3));
     R11 = 1 + rt1.*u(:,1,:).*u(:,1,:)+rt2.*v(:,1,:).*v(:,1,:)+rt3.*v(:,1,:).*u(:,1,:);
     R12 = rt1.*u(:,1,:).*u(:,2,:)+rt2.*v(:,1,:).*v(:,2,:)+rt3.*v(:,1,:).*u(:,2,:);
     R13 = rt1.*u(:,1,:).*u(:,3,:)+rt2.*v(:,1,:).*v(:,3,:)+rt3.*v(:,1,:).*u(:,3,:);
     R21 = rt1.*u(:,2,:).*u(:,1,:)+rt2.*v(:,2,:).*v(:,1,:)+rt3.*v(:,2,:).*u(:,1,:);
     R22 = 1 + rt1.*u(:,2,:).*u(:,2,:)+rt2.*v(:,2,:).*v(:,2,:)+rt3.*v(:,2,:).*u(:,2,:);
     R23 = rt1.*u(:,2,:).*u(:,3,:)+rt2.*v(:,2,:).*v(:,3,:)+rt3.*v(:,2,:).*u(:,3,:);
     R31 = rt1.*u(:,3,:).*u(:,1,:)+rt2.*v(:,3,:).*v(:,1,:)+rt3.*v(:,3,:).*u(:,1,:);
     R32 = rt1.*u(:,3,:).*u(:,2,:)+rt2.*v(:,3,:).*v(:,2,:)+rt3.*v(:,3,:).*u(:,2,:);
     R33 = 1 + rt1.*u(:,3,:).*u(:,3,:)+rt2.*v(:,3,:).*v(:,3,:)+rt3.*v(:,3,:).*u(:,3,:);
     R(:,:,idx) = [R11 R12 R13; R21 R22 R23; R31 R32 R33];
end
end
function m = bsxRz(m,theta)
%% Purpose:
% Perform a rotation of theta radians about the z-axis on the vector(s) 
% described by m.
% 
%% Inputs:
% m         [N x 3]                                         vector matrix
%                                                           in which you
%                                                           would like to
%                                                           rotate with the
%                                                           x,y,z
%                                                           components
%                                                           specified along
%                                                           a specific
%                                                           dimension
%
% theta     [N x 1]                                         Rotation Angle
%                                                           about z-axis 
%                                                           in radians
%
%% Outputs:
% m         [N x 3]
%
%% Revision History:
%  Darin C Koblick (C)                              Initially Created 2013
%% ---------------------- Begin Code Sequence -----------
%Assemble the rotation matrix
Rz = zeros(3,3,size(m,1));
Rz(1,1,:) = cos(theta);  Rz(1,2,:) = -sin(theta);
Rz(2,1,:) = sin(theta);  Rz(2,2,:) =  cos(theta); 
Rz(3,3,:) = 1;
%Dim33Multiply
m = Dim33Multiply(m,Rz);
end
