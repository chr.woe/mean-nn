%% This script implements the stand-alone algorithm of the mean-nearest neighbour motor mapping method
%This was developed during the Master Thesis of C. W0ERLE, Electrical Engineering, Karlsruhe Institute of Technology, 2023


%% The script implements a novel heuristic for  TMS-MEP motor mapping, inspired by 'smoothing' filters in image processing

%The script requires synchronized Stimulus-MEP triggered data from the Localite TMS neuronavigation system as well from the NIBS-signal processing Toolbox:
%'BEST-toolbox' developed by: Hassan, U., Pillen, S., Zrenner, C., & Bergmann, T. O. (2022). The Brain Electrophysiological recording & STimulation (BEST) toolbox. Brain Stimulation, 15(1), 109–115. https://doi.org/10.1016/j.brs.2021.11.017


clear all;
rng('shuffle');
 
%definitions based on experiment/virtual data
numb_of_max_eval = 20;    % similar to parameter 'm' in the thesis   
radius = 0.035;     %ROI radius
angle_range_h = 45; % relative range for alpha +- 45°    
rotWeight = 0.8;     %weighting factor alpha 
nn_numb = 10;    %similar to parameter 'k' in the thesis



%% load data
load('C:\Users\...\tranformExperimentalData\BESTData_Hotspot_sub_X');
S = readstruct("C:\Users\...\Triggerfile_mod_sub2.txt","FileType","xml");


%% transform data
for i=1:length(S.TriggerMarker)
    Trigger_Marker(i,:) = [S.TriggerMarker(i).Matrix4D.data00Attribute,S.TriggerMarker(i).Matrix4D.data01Attribute,S.TriggerMarker(i).Matrix4D.data02Attribute,S.TriggerMarker(i).Matrix4D.data03Attribute,S.TriggerMarker(i).Matrix4D.data10Attribute,S.TriggerMarker(i).Matrix4D.data11Attribute,S.TriggerMarker(i).Matrix4D.data12Attribute,S.TriggerMarker(i).Matrix4D.data13Attribute,S.TriggerMarker(i).Matrix4D.data20Attribute,S.TriggerMarker(i).Matrix4D.data21Attribute,S.TriggerMarker(i).Matrix4D.data22Attribute,S.TriggerMarker(i).Matrix4D.data23Attribute];
end

MEP = (BESTData.Results.FDIr.MEPAmplitude/1000).';
[valMaxMEP, idxMaxMEP] = maxk(MEP, numb_of_max_eval);

for m = 1:numb_of_max_eval
    for n=1:12
        target_matrix(n).Value = Trigger_Marker(idxMaxMEP(m),n);
    end
    
   %% call transformation of the different coil positions to make them comparable to the hotspot-position in a 2D plane (slight error in E-Field)
    
    for i = 1:length(Trigger_Marker);
        double_matrix = Trigger_Marker(i,:).';
        
        %% 3D - 2D Transformations
        [dist(m), rot(m), direct(m), max_ang(m)] = transform_marker_matrix_3D(double_matrix, target_matrix , plot_example, numb);  %
        angle_shift(m, i) = max_ang(m);
        distance(m, i) = dist(m)/1000;
        rotation(m, i) = rot(m); %  notation is +angle --> conter clock wise starting at e1 = (1|0)
        direction(m, i) = direct(m); % notation is +angle --> conter clock wise starting at e1 = (1|0)

         %%  Calc nn_heuristic here
        relEucDis(m, i) = abs(distance(m, i))/radius*100; %in percent
        relAngleDeviation(m, i) = abs(rotation(m, i))/angle_range_h*100;%in percent
        combined_Deviation(m, i) = rotWeight*relAngleDeviation(m, i)+relEucDis(m, i);

    end 
    average_angle_shift_norm_vec(m) = mean(angle_shift(m));
end

%% Interpolation and Search for Max here
[nearest_val, nearest_idx] = mink(combined_Deviation, nn_numb+1, 2); % 10 nearest neighbours ATTENTION: first column is the tested max MEP index

for k = 1:numb_of_max_eval  
        average_MEP(k) = sum(MEP(nearest_idx(k, 1:nn_numb+1)))/(nn_numb+1);     
end
[best_MEP_set_av_val, best_MEP_set_idx] = max(average_MEP);
%%


%% Create a function second order to interpolate Head surface
x_for_interpol = Trigger_Marker(:, 4);
y_for_interpol = Trigger_Marker(:, 8);
z_for_interpol = Trigger_Marker(:, 12);
splinefit = fit([x_for_interpol, y_for_interpol],z_for_interpol,'poly22');

%% Interpolate best hot spot position and rotation 3D
interpolated_coord_x = sum(x_for_interpol(nearest_idx(best_MEP_set_idx, 1:nn_numb+1)))/(nn_numb+1);
interpolated_coord_y = sum(y_for_interpol(nearest_idx(best_MEP_set_idx, 1:nn_numb+1)))/(nn_numb+1);
z_from_spline_interpolation = splinefit(interpolated_coord_x, interpolated_coord_y);


%% interpolate coil rotation in 3D
head_vec_1 = Trigger_Marker(:, 1);
head_vec_2 = Trigger_Marker(:, 5);
head_vec_3 = Trigger_Marker(:, 9);
hand_vec_1 = Trigger_Marker(:, 2);
hand_vec_2 = Trigger_Marker(:, 6);
hand_vec_3 = Trigger_Marker(:, 10);
right_vec_1 = Trigger_Marker(:, 3);
right_vec_2 = Trigger_Marker(:, 7);
right_vec_3 = Trigger_Marker(:, 11);

interpolated_head_vec_1 = sum(head_vec_1(nearest_idx(best_MEP_set_idx, 1:nn_numb+1)))/(nn_numb+1);
interpolated_head_vec_2 = sum(head_vec_2(nearest_idx(best_MEP_set_idx, 1:nn_numb+1)))/(nn_numb+1);
interpolated_head_vec_3 = sum(head_vec_3(nearest_idx(best_MEP_set_idx, 1:nn_numb+1)))/(nn_numb+1);

interpolated_hand_vec_1 = sum(hand_vec_1(nearest_idx(best_MEP_set_idx, 1:nn_numb+1)))/(nn_numb+1);
interpolated_hand_vec_2 = sum(hand_vec_2(nearest_idx(best_MEP_set_idx, 1:nn_numb+1)))/(nn_numb+1);
interpolated_hand_vec_3 = sum(hand_vec_3(nearest_idx(best_MEP_set_idx, 1:nn_numb+1)))/(nn_numb+1);

interpolated_right_vec_1 = sum(right_vec_1(nearest_idx(best_MEP_set_idx, 1:nn_numb+1)))/(nn_numb+1);
interpolated_right_vec_2 = sum(right_vec_2(nearest_idx(best_MEP_set_idx, 1:nn_numb+1)))/(nn_numb+1);
interpolated_right_vec_3 = sum(right_vec_3(nearest_idx(best_MEP_set_idx, 1:nn_numb+1)))/(nn_numb+1);


%% All the Plotting
%----------------------------------------------------------------------------------------------------------------------
%----------------------------------------------------------------------------------------------------------------------
%% Plot spline interpolation + data points
figure(1)
plot(splinefit ,[x_for_interpol,y_for_interpol],z_for_interpol);
hold on;
scatter3(interpolated_coord_x, interpolated_coord_y, z_from_spline_interpolation, 'blue', 'filled');
scatter3(x_for_interpol(nearest_idx(best_MEP_set_idx, 1:nn_numb+1)), y_for_interpol(nearest_idx(best_MEP_set_idx, 1:nn_numb+1)), z_for_interpol(nearest_idx(best_MEP_set_idx, 1:nn_numb+1)), 'yellow', 'filled');
title = 'Spline Interpolated Stim-Sites';
hold off;

figure(2)
%plot(splinefit ,[x_for_interpol,y_for_interpol],z_for_interpol);
scatter3(x_for_interpol,y_for_interpol,z_for_interpol, 'black');
hold on;
scatter3(interpolated_coord_x, interpolated_coord_y, z_from_spline_interpolation, 'blue', 'filled');
scatter3(x_for_interpol(nearest_idx(best_MEP_set_idx, 1:nn_numb+1)), y_for_interpol(nearest_idx(best_MEP_set_idx, 1:nn_numb+1)), z_for_interpol(nearest_idx(best_MEP_set_idx, 1:nn_numb+1)), 'yellow', 'filled');
plot3([interpolated_coord_x interpolated_head_vec_1+interpolated_coord_x], [interpolated_coord_y interpolated_head_vec_2+interpolated_coord_y], [z_from_spline_interpolation interpolated_head_vec_3+z_from_spline_interpolation],'r-^', 'LineWidth',1.5);
plot3([interpolated_coord_x interpolated_hand_vec_1+interpolated_coord_x], [interpolated_coord_y interpolated_hand_vec_2+interpolated_coord_y], [z_from_spline_interpolation interpolated_hand_vec_3+z_from_spline_interpolation],'g-^', 'LineWidth',1.5);
plot3([interpolated_coord_x interpolated_right_vec_1+interpolated_coord_x],[interpolated_coord_y interpolated_right_vec_2+interpolated_coord_y],[z_from_spline_interpolation interpolated_right_vec_3+z_from_spline_interpolation],'b-^', 'LineWidth',1.5);
title = 'Relevant nn-positions and interpolated hotspot coil orientation and position';
hold off;

%% Plot the experiment data arround nn-best fit
x_plot = distance(best_MEP_set_idx, :);
y_plot = rotation(best_MEP_set_idx, :);
z_plot = MEP;
c = sqrt(z_plot);

figure(3)
scatter3(x_plot,y_plot,z_plot, 30 , c, 'filled');
title ='MEP-distribution depending on eucl. distance and rotation';
xlabel('Eucl. distance from center of nn-best fit (in m)')
ylabel('Rotation angle compared to target position (in °)')
zlabel('MEP (peak to peak in mV)')

figure(4)
scatter3(x_for_interpol,y_for_interpol,z_for_interpol, 30 , c, 'filled');
title= 'MEP-distribution depending on x, y, z';
xlabel('x (in mm)')
ylabel('y (in mm)')
zlabel('z (in mm)')

%% plot arround abs maximum


% x_plot = distance(idxMaxMEP, :);
% y_plot = rotation(idxMaxMEP, :);
% z_plot = MEP;
% c = sqrt(z_plot);
% 
% figure(5)
% scatter3(x_plot,y_plot,z_plot, 30 , c, 'filled');
% title ='MEP-distribution depending on eucl. distance and rotation';
% xlabel('Eucl. distance from center of nn-best fit (in m)')
% ylabel('Rotation angle compared to target position (in °)')
% zlabel('MEP (peak to peak in mV)')


%% preprocessing for Plotting 2D data

%Set some plotting parameters
x_roi_spot = 0.0; %set target/interpolated spot relative on zero 
y_roi_spot = 0.0;

%renormation of initial random Stimulation Position
StimPos(:,1) = distance(best_MEP_set_idx, :);
StimPos(:,2) = direction(best_MEP_set_idx, :)/360*2*pi; 
StimPos(:,3) = rotation(best_MEP_set_idx, :)/360*2*pi;

%Transform Position to Kartesian
% PI/2 addiert für Veranschaulichung   
[x_rand_pos, y_rand_pos] = pol2cart(StimPos(:,2)+pi/2, StimPos(:,1)); 
%Transform angle to Kartesian
[U, V] = pol2cart(StimPos(:,3)+pi/2, 0.007); 
rel_hotspot_x = (x_for_interpol(1) - interpolated_coord_x)/1000;
rel_hotspot_y = (y_for_interpol(1) - interpolated_coord_y)/1000;
interpolated_rotation_2D_hotspot = sum(StimPos(nearest_idx(best_MEP_set_idx, 1:nn_numb+1), 3))/(nn_numb+1);
[T, Z] = pol2cart(interpolated_rotation_2D_hotspot/360*2*pi+pi/2, 0.007);

%% Plot the ROI with the different coil positions and orientations in 2D

figure(6)
s = quiver(-x_rand_pos, -y_rand_pos, U, V, 0);
s.LineWidth = 1;
s.Color = 'blue';
s.Marker = '.';
axis equal;
hold on;
plot(x_roi_spot, y_roi_spot, '*', 'MarkerSize', 12, 'LineWidth', 2);

w = quiver(rel_hotspot_x, rel_hotspot_x, T, Z, 0);
w.LineWidth = 1;
w.Color = 'green';
w.Marker = 'h';
w.MarkerSize = 10;

% axis([-0.06 0.06 -0.06 0.06]);
% axis equal;

%plot ROI
% th = 0:pi/360:2*pi;
% xunit = radius * cos(th) + x_roi_spot;
% yunit = radius * sin(th) + y_roi_spot;
% plot(xunit, yunit, 'Color', 'yellow');
% plot3(x_rand_pos, y_rand_pos, MEP, 'x', 'MarkerSize', 7);
hold off;











