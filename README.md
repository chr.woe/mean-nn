# Mean-nearest neighbour algorithm
This project includes the source code of the mean-nearest neighbour motor mapping method (Matlab R2022 based), for precise 'hotspot' search in Transcranial magnetic stimulation (TMS) - motor evoked potential (MEP) experiments. The algorithm will interpolate a optimal coil position and orientation based on a set of pseudo-randomized acquired TMS induced MEPs.

Additionally, this project includes a novel virtual model, which enables a virtual test bench for motor mapping procedures.


## Getting started with the mean-nearest neighbour algorithm
The Mean-nearest neighbour aglorithm is a stand-alone implementation of a novel 'hotspot' motor mapping method. 
It is implemented in the mean_nn.m file which calls helper function for geometric complexity reduction.

Required input data: a Triggermarker file from the neuronavigation system (Localite). This includes all the coordinate information from each stimulation site.
 Additionally, peak-to-peak MEP amplitudes already extracted from the electromyography raw data stream are required as input. Here the Non-Invasive Brain Stimulation signal processing toolbox BEST-toolbox (Hassan et al. 2022) was used for this purpose. In principle the mean-nn algorithm can be adapted for the usage of other neuronavigation software and signal processing tools.


## Getting started with the virtual model
Additionally, a virtual model is provided which enables virtual testing of motor mapping methods during the development process. It can be a helpful tool for the validation of novel methods, before testing them on data from patients/subjects.

The stimulate_random.m script includes parts of the work from (Goetz et al. 2019) and (Makarov et al. 2020). Therefore, both software repositories from the cited literature need to be installed/apdapted as instructed in the original repositories (see the respective URLs below). To use the implemented virtual model, the scripts provided here (stimulate_random.m ;  call_E_field.m), need to be embedded within the installed software packages (mainly, the Matlab paths need to be added/aligned).




# References and used external software:
## Virtual model:

The used BEM-FMM E-field simulation approach was adapted and modified from: 
Makarov SN, Wartman WA, Daneshzand M, Fujimoto K, Raij T, Nummenmaa A. A software toolkit for TMS electric-field modeling with boundary element fast multipole method: an efficient MATLAB implementation. J Neural Eng. 2020 Aug 4;17(4):046023. doi: 10.1088/1741-2552/ab85b3. PMID: 32235065.
See also: https://tmscorelab.github.io/TMS-Modeling-Website/

The statistical model of MEPs was adapted from:  
Goetz, S. M., Alavi, S. M. M., Deng, Z.-D., & Peterchev, A. V. (2019).
Statistical Model of Motor-Evoked Potentials. IEEE Transactions on Neural Systems and Rehabilitation Engineering, 27(8), 1539–1545. 
https://doi.org/10.1109/TNSRE.2019.2926543
See also: https://github.com/sgoetzduke/Statistical-MEP-Model


## Mean-nearest neighbour implementation
BEST-toolbox:
Hassan, U., Pillen, S., Zrenner, C., & Bergmann, T. O. (2022). The Brain Electrophysiological recording & STimulation (BEST) toolbox. Brain Stimulation, 15(1), 109–115. https://doi.org/10.1016/j.brs.2021.11.017

Geometric complexity reduction used code found online:
Darin Koblick (2023). rotVecAroundArbAxis(unitVec2Rotate,rotationAxisUnitVec,theta) (https://www.mathworks.com/matlabcentral/fileexchange/49916-rotvecaroundarbaxis-unitvec2rotate-rotationaxisunitvec-theta), MATLAB Central File Exchange. Retrieved February 5, 2023. 

and: 
https://de.mathworks.com/matlabcentral/answers/501449-angle-betwen-two-3d-vectors-in-the-range-0-360-degree, author James Tursa, 23 Jan 2020








